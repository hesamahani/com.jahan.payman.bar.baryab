package com.jahan.payman.bar.baryab.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jahan.payman.bar.baryab.R;
import com.jahan.payman.bar.baryab.customviews.CustomToast;
import com.jahan.payman.bar.baryab.dialogs.ConfirmDialog;
import com.jahan.payman.bar.baryab.dialogs.MessageDialog;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ConfirmDialog confirmDialog = new ConfirmDialog(SplashActivity.this, "بار یاب", "سلام برادر");
        confirmDialog.setOnClickListener(new ConfirmDialog.OnClickListener() {
            @Override
            public void onConfirm() {
                CustomToast.makeText(SplashActivity.this, "dsdsd", CustomToast.LENGTH_LONG, CustomToast.TYPE_INFO);
            }

            @Override
            public void onCancel() {
                CustomToast.makeText(SplashActivity.this, "dsdsd", CustomToast.LENGTH_LONG, CustomToast.TYPE_ERROR);
            }
        });
        confirmDialog.show();
    }
}