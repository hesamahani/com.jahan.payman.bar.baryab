package com.jahan.payman.bar.baryab.helpers;

import android.content.Context;

/**
 * Created by Hesam Aldin Ahani on 10/10/2017.
 */

public class ActivitiesHelpers {
    private static ActivitiesHelpers activitiesHelpers;
    private Context context;

    public ActivitiesHelpers(Context context) {
        this.context = context;
    }

    public static ActivitiesHelpers getInstance(Context context) {
        if (activitiesHelpers == null)
            activitiesHelpers = new ActivitiesHelpers(context);

        return activitiesHelpers;
    }
}
